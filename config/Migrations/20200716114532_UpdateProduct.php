<?php
use Migrations\AbstractMigration;

class UpdateProduct extends AbstractMigration
{

    public function change()
    {

        $table = $this->table("products")

            ->addColumn('description_ar', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])

            ->addColumn('pro_available', 'boolean', [
                'default' => false,
                'null' => true,
            ])

            ->addColumn('parent_id', 'integer', [
                'limit' => 11,
                'null' => true,
            ])

            ->addColumn('lft', 'integer', [
                'limit' => 11,
                'null' => false,
            ])

            ->addColumn('rght', 'integer', [
                'limit' => 11,
                'null' => false,
            ]);


        $table->update();

    }

}

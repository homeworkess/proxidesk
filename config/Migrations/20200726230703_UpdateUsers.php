<?php
use Migrations\AbstractMigration;

class UpdateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table("users")

            ->addColumn('phone_validation', 'boolean', [
                'default' => false,
                'null' => true,
            ]);

        $table->update();
    }
}

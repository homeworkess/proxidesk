<?php

namespace Authentication\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Auth\PasswordHasherFactory;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class WadjbaFrontalAuthenticate extends BaseAuthenticate
{

    /* Authentification Hybride Soit username / pass OU identity / pin_code */
    public function authenticate(ServerRequest $request, Response $response)
    {
        if($request->getData('username',false) && $request->getData('password',false)){

            /* Recherche de l'utilisateur */
            $user = TableRegistry::get('Authentication.Users')
                ->find('all')
                ->where([
                    [
                        'identity' => trim( $request->getData('username' ))
                    ],
                    'active' => true
                ])
                ->contain(['Companies' => ['CompanyTypes'] ])
                ->first();

            /* DEBUG */
            // $password = "vegetek";
            // $hasher = new DefaultPasswordHasher();
            
            // debug($hasher->hash($password));
            // exit;
            //return $user->toArray();    

            /* Vérification du mot de passe */
            if(
                ($user && (new DefaultPasswordHasher)->check( trim($request->getData('password')), $user->password))
            ){
                return $user->toArray();
            }

        }
         return false;
    }


}
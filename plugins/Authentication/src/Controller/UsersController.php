<?php
namespace Authentication\Controller;

use Authentication\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Text;


class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login','logout']);
    }

    public function login()
    {

        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if ($user) {

                $nameSpace = 'Admin';
                $this->Auth->sessionKey ="Auth.$nameSpace";
                $this->Auth->setUser($user);

                /* @todo le client doit choisir entre la partie entreprise ou grand public */
                //return $this->redirect('/switch/index');
                return $this->redirect([
                    'plugin' => $nameSpace,
                    'controller' => 'Dashboard',
                    'action' => 'index'
                ]);

            }

            $this->Flash->error(__('Utilisateur ou mot de passe incorrect'));

        }

    }

    public function logOut(){
        $this->request->getSession()->destroy();
        return $this->redirect([
            'plugin' =>'Authentication',
            'controller' => 'Users',
            'action' => 'login'
        ]);
    }

}

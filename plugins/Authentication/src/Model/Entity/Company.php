<?php
namespace Authentication\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property string $denomination
 * @property string $type
 * @property string $address
 * @property float $longitude
 * @property float $latitude
 * @property string $city
 * @property string $rc_n
 * @property string $iden_fiscal
 * @property string $phone
 * @property string $mobile
 * @property int $user_id
 * @property int $account_id
 * @property int $uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Unique $unique
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Employee[] $employees
 * @property \App\Model\Entity\Order[] $orders
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'denomination' => true,
        'type' => true,
        'address' => true,
        'longitude' => true,
        'latitude' => true,
        'city' => true,
        'rc_n' => true,
        'iden_fiscal' => true,
        'phone' => true,
        'mobile' => true,
        'user_id' => true,
        'account_id' => true,
        'uid' => true,
        'created' => true,
        'modified' => true,
        'unique' => true,
        'user' => true,
        'account' => true,
        'employees' => true,
        'orders' => true
    ];


    protected $_hidden = [
        'created','modified'
    ];


}

<?php
namespace Authentication\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CompanyTypes Model
 *
 * @property \Authentication\Model\Table\CompaniesTable|\Cake\ORM\Association\HasMany $Companies
 *
 * @method \Authentication\Model\Entity\CompanyType get($primaryKey, $options = [])
 * @method \Authentication\Model\Entity\CompanyType newEntity($data = null, array $options = [])
 * @method \Authentication\Model\Entity\CompanyType[] newEntities(array $data, array $options = [])
 * @method \Authentication\Model\Entity\CompanyType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Authentication\Model\Entity\CompanyType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Authentication\Model\Entity\CompanyType[] patchEntities($entities, array $data, array $options = [])
 * @method \Authentication\Model\Entity\CompanyType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompanyTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('company_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Companies', [
            'foreignKey' => 'company_type_id',
            'className' => 'Authentication.Companies'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}

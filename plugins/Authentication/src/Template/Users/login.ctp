<!--<video  id="bgvid" playsinline autoplay muted loop>
    <source src="authentication/videos/login.mp4" type="video/mp4">
</video>
-->

<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

<section id="wrapper" class="new-login-register">
    <div class="new-login-box">

 <!--       <div class="logo text-center">
            <img width="80%" src="<?/*= $this->Url->build("/img/logo.svg") */?>" />
            <br />
            <br />
        </div>-->

        <div class="white-box">


            <?= $this->Form->create(null,['class'=>'form-horizontal new-lg-form','id'=>'loginform','autocomplete'=>"off"]) ?>

            <?= $this->Flash->render() ?>

            <div class="form-group">
                <div class="col-xs-12">
                    <h5><strong>utilisateur</strong></h5>
                    <input class="form-control" type="text" required="" placeholder="EMAIL OU PID" autocomplete="off" name="username">
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-12">
                    <h5><strong>Mot de passe</strong></h5>
                    <input class="form-control" type="password" required="" placeholder="Code PIN" name="password" autocomplete="off">
                </div>
            </div>

            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-wj btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Connexion</button>
                </div>
            </div>

            <?= $this->Form->end() ?>

            <?= $this->Form->create(null,['class'=>'form-horizontal','id'=>'recoverform','url'=>['controller'=>'users','action'=>'recover']]) ?>

            <div class="form-group ">
                <div class="col-xs-12">
                    <h3>Récupérez votre compte</h3>
                    <p class="text-muted">Enterez votre adresse Email des instructions vous seront envoyées! </p>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <input class="form-control" type="email" name="username" required="" placeholder="Email">
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Envoyer</button>
                </div>
            </div>

            <?= $this->Form->end() ?>

        </div>

        <p style="margin-top: 15px; color:white" class="text-center">Proxidesk</p>

    </div>


</section>



<style type="text/css">


    .new-login-register .new-login-box {

        margin: 0;
        position: absolute;
        left: 50%;
        top:50%;
        transform: translate(-50%,-50%);

    }

    .new-login-register {

        /*background: url(*/<?//= $this->Url->build('/img/log-in.jpg') ?>/*);*/
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;

    }

    body, html {

        background: #7C3D97 !important;

    }

    .white-box {

        margin: 0 !important;
        padding-bottom: 15px !important;
        border-radius: 4px;

    }

    .new-login-register .new-login-box .new-lg-form {
        padding-top: 0 !important;
    }

    .logo {

        margin-bottom: 15px;

    }

    video {
        position: fixed;
        top: 50%;
        left: 50%;
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        z-index: -100;
        transform: translateX(-50%) translateY(-50%);
        /*background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;*/
        background-size: cover;
        transition: 1s opacity;
    }



</style>


<!-- jQuery -->
<?= $this->Html->script('/ample/plugins/bower_components/jquery/dist/jquery.min.js') ?>

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>
<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom') ?>
<!--Style Switcher -->
<?= $this->Html->script('/ample/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') ?>

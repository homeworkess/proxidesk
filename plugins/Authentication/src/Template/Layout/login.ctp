<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/ample/plugins/images/favicon.png">
    <title>PROXIDESK</title>

    <!-- Bootstrap Core CSS -->
    <?= $this->Html->css('/ample/bootstrap/dist/css/bootstrap.min.css') ?>

    <!-- animation CSS -->
    <?= $this->Html->css('/ample/css/animate.css') ?>

    <!-- Custom CSS -->
    <?= $this->Html->css('/ample/css/style.css') ?>

    <!-- color CSS -->
    <?= $this->Html->css('/ample/css/colors/default.css',['id'=>"theme"]) ?>

    <!-- CSS Wadjba -->
    <?= $this->Html->css('/ample/css/wajba-admin.css') ?>

    <?= $this->fetch('scripts_header') ?>

    <?= $this->fetch('css_header') ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Contenu principal -->
<?= $this->fetch('content') ?>

<!-- JS FOOTER -->
<?= $this->fetch('footer-js-assets') ?>

</body>

</html>

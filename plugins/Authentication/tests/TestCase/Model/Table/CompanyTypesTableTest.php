<?php
namespace Authentication\Test\TestCase\Model\Table;

use Authentication\Model\Table\CompanyTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Authentication\Model\Table\CompanyTypesTable Test Case
 */
class CompanyTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Authentication\Model\Table\CompanyTypesTable
     */
    public $CompanyTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.authentication.company_types',
        'plugin.authentication.companies',
        'plugin.authentication.users',
        'plugin.authentication.roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyTypes') ? [] : ['className' => CompanyTypesTable::class];
        $this->CompanyTypes = TableRegistry::get('CompanyTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

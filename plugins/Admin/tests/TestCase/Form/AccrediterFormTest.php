<?php
namespace Admin\Test\TestCase\Form;

use Admin\Form\AccrediterForm;
use Cake\TestSuite\TestCase;

/**
 * Admin\Form\AccrediterForm Test Case
 */
class AccrediterFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Form\AccrediterForm
     */
    public $Accrediter;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Accrediter = new AccrediterForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Accrediter);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

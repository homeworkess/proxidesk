<?php
namespace Admin\Test\TestCase\Model\Behavior;

use Admin\Model\Behavior\UserLoggerBehavior;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Behavior\UserLoggerBehavior Test Case
 */
class UserLoggerBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Model\Behavior\UserLoggerBehavior
     */
    public $UserLogger;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->UserLogger = new UserLoggerBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserLogger);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\CompanyTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\CompanyTypesTable Test Case
 */
class CompanyTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Model\Table\CompanyTypesTable
     */
    public $CompanyTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.company_types',
        'plugin.admin.companies',
        'plugin.admin.users',
        'plugin.admin.accounts',
        'plugin.admin.cards',
        'plugin.admin.card_types',
        'plugin.admin.card_operations',
        'plugin.admin.employees',
        'plugin.admin.orders',
        'plugin.admin.order_lines'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyTypes') ? [] : ['className' => CompanyTypesTable::class];
        $this->CompanyTypes = TableRegistry::get('CompanyTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Admin\Test\TestCase\Controller\Component;

use Admin\Controller\Component\CompanyAccountPasswordHelperComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Controller\Component\CompanyAccountPasswordHelperComponent Test Case
 */
class CompanyAccountPasswordHelperComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Controller\Component\CompanyAccountPasswordHelperComponent
     */
    public $CompanyAccountPasswordHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->CompanyAccountPasswordHelper = new CompanyAccountPasswordHelperComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyAccountPasswordHelper);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

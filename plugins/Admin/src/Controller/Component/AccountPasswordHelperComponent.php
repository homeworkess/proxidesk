<?php
namespace Admin\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * CompanyAccountPasswordHelper component
 */
class AccountPasswordHelperComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function generatePassword($length) {
        $string = "";
        for($i=0; $i < $length; $i++) {
            $x = mt_rand(0, 3);
            switch($x) {
                case 0: $string.= chr(mt_rand(97,102));break;
                case 1: $string.= chr(mt_rand(65,90));break;
                case 2: $string.= chr(mt_rand(48,57));break;
                case 3: $string.= chr(mt_rand(103,122));break;
            }
        }
        return $string;
    }


    public function generateCardSerial($length){
        return $this->generatePassword($length);
    }


    /* GENERE UN ID UNIQUE POUR CHAQUE CLIENT */
    public function getRandomNum()
    {
        return str_pad(mt_rand(1,999999),6,'0',STR_PAD_LEFT);
    }


}

<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Dashboard Controller
 *
 *
 * @method \Admin\Model\Entity\Dashboard[] paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.dashboard');
    }

    public function index(){}

}

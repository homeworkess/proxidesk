<?php
namespace Admin\Controller;

use Admin\Controller\AppController;
use Admin\Form\AccrediterForm;
use Cake\ORM\TableRegistry;

/**
 * Companies Controller
 *
 *
 * @method \Admin\Model\Entity\Company[] paginate($object = null, array $settings = [])
 */
class CompaniesController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.company_edit');
    }

    public function index($type)
    {
        $companies = $this->Companies
            ->find('all')
            ->contain(['Users','Accounts','CompanyTypes'])
            ->where(['CompanyTypes.name'=>$type ])
            ->order([
                'Companies.id' => 'DESC'
            ]);

        $this->set(compact('companies'));
        $this->render($type);

    }

    public function view($id = null)
    {

        $company = $this->Companies->get($id, [
            'contain' => ['Users','Accounts','CompanyTypes']
        ]);

        $this->set('company', $company);
    }

    public function add($type)
    {

        $this->loadComponent('Admin.AccountPasswordHelper');

        $company = $this->Companies->newEntity();

        if ($this->request->is('post')) {

            $company = $this->Companies->patchEntity($company, $this->request->getData());


            /* IDENTIFIANT UNIQUE DE L'ENTREPRISE */
            $company->unique_id = strtoupper($this->AccountPasswordHelper->generateCardSerial(15));

            if ($this->Companies->save($company)) {

                $this->Flash->success(strtoupper(__('Le client a été correctement ajouté')));
                return $this->redirect(['action'=>'index',$type]);

            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de lajout du client')));

        }

        /* POUR LE USER ID GENERER UN NUMERO SUR 6 CHIFFRES DONE */
        $this->set('identity',$this->AccountPasswordHelper->getRandomNum());

        /* POUR LE PIN CODE GENERER UN NUMERO SUR 6 CHIFFRES DONE */
        $this->set('pin_code',$this->AccountPasswordHelper->getRandomNum());

        /* CATEGORIE POUR LA REDIRECTION ET L'AFFICHE D'ELEMENTS SUR LE FORMULAIRE */
        $this->set('cat',$type);

        $this->set(compact('company'));

    }

    public function edit($id = null)
    {
        $company = $this->Companies->get($id, ['contain' => ['Users','CompanyTypes', 'Accounts'=>'Cards'] ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $company = $this->Companies->patchEntity($company, $this->request->getData());
            $this->Companies->save($company);

            if ($this->Companies->save($company)) {

                $this->Flash->success(__(strtoupper('Les nouvelles informations ont été enregistrées')));
                return $this->redirect(['action'=>'index',$company->company_type->name]);

            } else {

                $this->Flash->error(__(strtoupper("Une erreur est servenue l'ors de la modification")));

            }


        }
        $this->set(compact('company'));
    }

    /* Accrediter le compte du client */
    public function accrediter($id)
    {
        $accreditationForm = new AccrediterForm();

        if($this->request->is('post')){

            /* Passer la Transaction */
            if($accreditationForm->execute($this->request->getData())){

                $amount = (int)str_replace(' ','',$this->request->getData('amount'));

                if($this->Companies->accrediter($id,$amount)){

                    $this->Flash->success(__("Le compte de l'entreprise a été correctement accrédité"));
                    return $this->redirect([
                        'action'=>'index','company'
                    ]);

                }

                $this->Flash->error(__("Erreur l'ors de l'accréditation"));
                return $this->redirect($this->referer());

            }
        }


        $this->set('accForm',$accreditationForm);
    }

    /* Ouverture de session  */
    public function login($companyID)
    {

        $user = $this->Companies->Users
            ->find('all')
            ->contain(['Companies' => ['CompanyTypes']])
            ->where(['Companies.id' => $companyID ])
            ->first()
            ->toArray();

        /* FIX DIFFERENCE HOTEL ET RESTO */
        if($user['company']['company_type']['name'] == 'hotel')
            $user['company']['company_type']['name'] = 'restaurant';

        $this->request->getSession()->write('Auth.'.ucfirst($user['company']['company_type']['name']),$user);

        $this->redirect([
            'plugin'     => ucfirst($user['company']['company_type']['name']),
            'controller' => 'Dashboard',
            'action'     => 'index',
        ]);

    }

}

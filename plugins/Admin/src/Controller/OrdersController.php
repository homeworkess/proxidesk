<?php
namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 *
 * @method \Admin\Model\Entity\Order[] paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.dashboard');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $orders = $this->Orders
            ->find('all',[ 'order' => ['Orders.created' => 'DESC'] ])
            ->contain(['Companies','OrderLines'])
            ->where(['ISNULL(origin)']);

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }


    public function process($id,$status){

        $order = $this->Orders->get($id,[
            'contain' => ['OrderLines']
        ]);

        $order->status = $status;

        if($status == 'valid'){

            $total = 0;

            /* Faire la somme des lignes produits de type credit waps */
            foreach ($order->order_lines as $line){
                if($line->product_id == 1){
                    $total += (int)$line->qty;
                }
            }

            /* Alimenter le compte de l'entreprise */
            $company = TableRegistry::get('Admin.Companies')
                ->get($order->company_id,['contain'=>'Accounts']);

            $company->account->credit += (int)$total;


            /* Garder l'information dans ordered_waps */
            $destiAccount = TableRegistry::get('Admin.Accounts')->get(
                $this->request->getSession()->read('Auth.Admin.company.account_id')
            );
            $destiAccount->ordered_waps += (int)$total;

            /* Persister le statut du bon de commande */
            TableRegistry::get('Admin.Accounts')->save($company->account);

            /* Persister l'information de la quantité de la commande */
            TableRegistry::get('Admin.Accounts')->save($destiAccount);

        }

        $this->Orders->save($order);
        return $this->redirect(['action' => 'index']);

    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['OrderLines']
        ]);

        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

}

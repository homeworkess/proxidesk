<?php

namespace Admin\Controller;

use App\Controller\AppController as BaseController;

class AppController extends BaseController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadComponent('Csrf');
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $session = $this->request->getSession()->read('Auth.Admin');
        return true;
//        if( $this->request->getSession()->check('Auth.Admin') && $session['is_super_admin'])
//            return true;
//        else

        return $this->redirect([
           'plugin'     => 'Authentication',
           'controller' => 'Users',
           'action'     => 'login'
        ]);
    }



}

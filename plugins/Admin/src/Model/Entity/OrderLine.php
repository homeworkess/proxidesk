<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderLine Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $qty
 * @property int $tickets_per_stack
 * @property int $facial_value
 * @property int $order_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Admin\Model\Entity\Product $product
 * @property \Admin\Model\Entity\Order $order
 */
class OrderLine extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'qty' => true,
        'tickets_per_stack' => true,
        'facial_value' => true,
        'order_id' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'order' => true
    ];
}

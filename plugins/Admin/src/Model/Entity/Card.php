<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Card Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $use_start
 * @property \Cake\I18n\FrozenTime $expiration
 * @property int $card_type_id
 * @property string $serial_number
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Admin\Model\Entity\CardType $card_type
 * @property \Admin\Model\Entity\Account[] $accounts
 * @property \Admin\Model\Entity\CardOperation[] $card_operations
 */
class Card extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}

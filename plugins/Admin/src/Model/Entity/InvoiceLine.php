<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * InvoiceLine Entity
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $product_id
 * @property int $qty
 * @property float $unit_price
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $uid
 *
 * @property \Admin\Model\Entity\Invoice $invoice
 * @property \Admin\Model\Entity\Product $product
 */
class InvoiceLine extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'invoice_id' => true,
        'product_id' => true,
        'qty' => true,
        'unit_price' => true,
        'created' => true,
        'modified' => true,
        'uid' => true,
        'invoice' => true,
        'product' => true
    ];
}

<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $denomination
 * @property string $address
 * @property string $city
 * @property string $rc_n
 * @property string $ai
 * @property string $if_id
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Admin\Model\Entity\If $if
 * @property \Admin\Model\Entity\User $user
 * @property \Admin\Model\Entity\Order[] $orders
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

}

<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Employee Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $email
 * @property string $mobile
 * @property int $user_id
 * @property int $company_id
 * @property int $account_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $uid
 *
 * @property \Admin\Model\Entity\User $user
 * @property \Admin\Model\Entity\Company $company
 * @property \Admin\Model\Entity\Account $account
 */
class Employee extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'address' => true,
        'email' => true,
        'mobile' => true,
        'user_id' => true,
        'company_id' => true,
        'account_id' => true,
        'created' => true,
        'modified' => true,
        'uid' => true,
        'user' => true,
        'company' => true,
        'account' => true
    ];
}

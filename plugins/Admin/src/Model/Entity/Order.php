<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $delivery_date
 * @property int $qty
 * @property int $company_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $status
 * @property string $remarks
 *
 * @property \Admin\Model\Entity\Company $company
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    protected $_virtual = ['ref','total_waps'];

    protected function _getRef(){
        return 'PO/'.$this->_properties['created']->i18nformat('yyM-d').$this->_properties['id'];
    }

    /* Calcul du crédit total des waps */
    protected function _getTotalWaps(){

        $total = 0;

        foreach ($this->_properties['order_lines'] as $order){
            if($order->product_id == 1)
                $total += $order->qty;
        }

        return $total;
    }

}

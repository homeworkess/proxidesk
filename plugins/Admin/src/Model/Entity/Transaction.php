<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $id
 * @property int $account_id
 * @property int $user_id
 * @property string $operation
 * @property float $amount
 * @property int $difference
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Admin\Model\Entity\Account $account
 * @property \Admin\Model\Entity\User $user
 * @property \Admin\Model\Entity\TransactionMeta[] $transaction_metas
 */
class Transaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'user_id' => true,
        'operation' => true,
        'amount' => true,
        'difference' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'account' => true,
        'user' => true,
        'transaction_metas' => true,
        'transaction_info' => true,
    ];
}

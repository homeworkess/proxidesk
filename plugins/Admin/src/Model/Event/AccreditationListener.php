<?php

namespace Admin\Model\Event;

use Cake\Chronos\Date;
use Cake\Core\Configure;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\Email;

class AccreditationListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Model.Company.accredited' => 'sendNotificationEmail',
        );
    }

    public function sendNotificationEmail($event) {

        /* Envoyer un mail */
        $msg = "Le compte de l'entreprise : ".$event->data['company_denomination']." <br />".
               "A été accredité de la somme de : ".$event->data['amount']." <br />".
               "Le : ".(new \DateTime())->format('d-m-Y H:m');

        $email = new Email('default');
        $email
            ->setTransport('gmail')
            ->setFrom(['transactions@wajba-dz.com' ])
            ->setTo(Configure::read('notification_email'))
            ->setSubject("ACCREDITATION"."-".$event->data['company_unique_id'])
            ->setEmailFormat('html')
            ///->setViewVars(array('msg' => "MON MESSAGE"))
            ->send($msg);

    }
}
<?php
namespace Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\ORM\Behavior;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Network\Session;

/**
 * Companies Model
 *
 * @property \Admin\Model\Table\IfsTable|\Cake\ORM\Association\BelongsTo $Ifs
 * @property \Admin\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \Admin\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 *
 * @method \Admin\Model\Entity\Company get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompaniesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Admin.UserLogger');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Users'
        ]);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
            'className' => 'Admin.Accounts'
        ]);

        $this->hasMany('Orders', [
            'foreignKey' => 'company_id',
            'className' => 'Admin.Orders'
        ]);

        $this->belongsTo('CompanyTypes', [
            'foreignKey' => 'company_type_id',
            'joinType' => 'INNER',
            'className' => 'Admin.CompanyTypes'
        ]);
    }




    /* Accrediter le compte d'une entreprise */
    public function accrediter(int $id,int $amount ):?bool
    {
        $session = new Session();

        $company = $this->get($id,['contain'=>['Accounts']]);
        $company->account->credit += (int)$amount;

        $transaction = TableRegistry::get('Admin.Transactions')->newEntity(
            [
                "account_id"  => $company->account->id,
                "user_id"     => $session->read('Auth.Admin.id'),
                "operation"   => 'deposit',
                "amount"      => (float)$amount,
                "difference"  => 0,
                "status"      => 'proceed',
                "transaction_info" => 'company_accreditation_deposit'
            ]
        );

        if( $this->Accounts->save($company->account) && TableRegistry::get('Admin.Transactions')->save($transaction) ){

            /* Envoyer un mail */
            $event = new Event('Model.Company.accredited', $this,[
                'action'     => 'company_accreditation_deposit',
                'amount'     => (float)$amount,
                'user_id'    => $session->read('Auth.Admin.id'),
                'company_id' => $company->id,
                'company_denomination' => $company->denomination,
                'company_unique_id' => $company->unique_id,

            ]);

            $this->getEventManager()->dispatch($event);

            return true;

        }

        return false;

    }







    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('denomination')
            ->requirePresence('denomination', 'create')
            ->notEmpty('denomination')
            ->add('denomination', [
                'length' => [
                    'rule' => ['minLength', 4],
                    'message' => '4 caractères au minimum',
                ]
            ]);

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('rc_n')
            ->allowEmpty('rc_n');

        $validator
            ->scalar('iden_fiscal')
            ->allowEmpty('iden_fiscal');

        $validator
            ->scalar('mobile')
            ->allowEmpty('mobile');

        $validator
            ->email('email')
            ->allowEmpty('email')
            ->add('email', [
                'length' => [
                    'rule' => 'email',
                    'message' => 'entrez un mail valide',
                ]
            ]);

        $validator
            ->numeric('commission')
            ->range('commission',[0,100],'Valeurs autorisées de 0 à 100');

        $validator
            ->numeric('discount')
            ->range('discount',[0,100],'Valeurs autorisées de 0 à 100');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['unique_id']));
        $rules->add($rules->isUnique(['denomination'],'Une entreprise existe déjà avec cette dénomination'));
        return $rules;
    }
}

<?php
namespace Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;

/**
 * Accounts Model
 *
 * @property \Admin\Model\Table\CardsTable|\Cake\ORM\Association\BelongsTo $Cards
 * @property \Admin\Model\Table\CompaniesTable|\Cake\ORM\Association\HasMany $Companies
 * @property \Admin\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 *
 * @method \Admin\Model\Entity\Account get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Account newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Account|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Account[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Account findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cards', [
            'foreignKey' => 'card_id',
            'className' => 'Admin.Cards'
        ]);
        $this->hasMany('Companies', [
            'foreignKey' => 'account_id',
            'className' => 'Admin.Companies'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'account_id',
            'className' => 'Admin.Employees'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('credit')
            ->allowEmpty('credit');

        $validator
            ->decimal('notifications_threshold')
            ->allowEmpty('notifications_threshold');

        $validator
            ->decimal('exceeding_allowed_credit')
            ->allowEmpty('exceeding_allowed_credit')
            ->nonNegativeInteger('exceeding_allowed_credit');

        $validator
            ->requirePresence('allow_card_use', 'create')
            ->notEmpty('allow_card_use');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['card_id'], 'Cards'));

        return $rules;
    }


    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {

        /* Conversion des masques de saisies */
        if(isset($data['credit']))
        {
            $data['credit'] = (float)str_replace(' ','',$data['credit']);
            $data['notifications_threshold'] = (float)str_replace(' ','',$data['notifications_threshold']);
            $data['exceeding_allowed_credit'] = (float)str_replace(' ','',$data['exceeding_allowed_credit']);
        }

        return true;

    }



}

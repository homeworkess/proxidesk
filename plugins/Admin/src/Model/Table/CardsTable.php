<?php
namespace Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cards Model
 *
 * @property \Admin\Model\Table\CardTypesTable|\Cake\ORM\Association\BelongsTo $CardTypes
 * @property \Admin\Model\Table\AccountsTable|\Cake\ORM\Association\HasMany $Accounts
 * @property \Admin\Model\Table\CardOperationsTable|\Cake\ORM\Association\HasMany $CardOperations
 *
 * @method \Admin\Model\Entity\Card get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Card newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Card[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Card|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Card patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Card[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Card findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CardTypes', [
            'foreignKey' => 'card_type_id',
            'joinType' => 'INNER',
            'className' => 'Admin.CardTypes'
        ]);
        $this->hasMany('Accounts', [
            'foreignKey' => 'card_id',
            'className' => 'Admin.Accounts'
        ]);
        $this->hasMany('CardOperations', [
            'foreignKey' => 'card_id',
            'className' => 'Admin.CardOperations'
        ]);
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {

        echo "GENERATION CODE CARTE";
        debug($entity);
        exit;


        if($entity->isNew()){
            $entity->user->role     = 'company';
            $entity->user->password = $entity->user->pin_code;
        }
        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('use_start')
            ->requirePresence('use_start', 'create')
            ->notEmpty('use_start');

        $validator
            ->dateTime('expiration')
            ->requirePresence('expiration', 'create')
            ->notEmpty('expiration');

        $validator
            ->scalar('serial_number')
            ->requirePresence('serial_number', 'create')
            ->notEmpty('serial_number');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['card_type_id'], 'CardTypes'));

        return $rules;
    }
}

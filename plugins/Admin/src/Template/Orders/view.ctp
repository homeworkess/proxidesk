<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">DETAILS DE LA COMMANDE</h4> </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->


            <?= $this->Form->create($order) ?>
            <div class="row">
                <div class="col-sm-10">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">COMMANDES</a></li>
                        <li class="active">DETAILS</li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <?= $this->Html->link('Retour',['action'=>'index'],['class'=>'btn btn-block btn-default pull-right'])  ?>
                </div>
            </div>

            <?= $this->Flash->render() ?>

            <div class="row" id="app">
                <div class="col-sm-12">

                    <div class="white-box">

                        <h3 class="box-title m-b-0">Informations sur le Bon de commande</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>


                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date de livraison :</label>
                                    <p><?= $order->delivery_date->i18nFormat('dd-MM-YYYY') ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Remarques :</label>
                                    <p><?= $order->remarks ?></p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input v-for="line in productLines" type="hidden" name="order_lines[]" :value='JSON.stringify(line)' />
                                </div>
                            </div>

                        </div>

                        <!-- Order Lines -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive m-t-40" style="clear: both;">

                                    <!-- Table Listing -->
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>PRODUIT</th>
                                            <th class="text-left">QTY</th>
                                            <th class="text-left">TICKETS / CARNET</th>
                                            <th class="text-left">VALEUR FACIALE</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr v-for="line in productLines">
                                            <td>{{ getProductName(line.product_id)}}</td>
                                            <td class="text-left">{{line.qty}}</td>
                                            <td class="text-left">{{line.tickets_per_stack}}</td>
                                            <td class="text-left">{{line.facial_value}}</td>
                                        </tr>

                                        </tbody>
                                    </table>





                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/bootstrap-select-1.12.4/dist/js/bootstrap-select.min',['block'=>'scripts_header']) ?>

<?= $this->Html->script('/ample/plugins/bower_components/moment/min/moment.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/moment-timezone/builds/moment-timezone-with-data.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>
<?= $this->Html->css('/ample/plugins/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>


<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/vue.min') ?>


<script type="text/javascript">


    var vm = new Vue({

        el : '#app',
        data : {

            productTypes : [
                {value:1,label:"Crédit WAPS"},
                {value:2,label:"Tickets Restaurant"}
            ],

            /* Lignes de produits */
            productLines : <?= json_encode($order->order_lines,true) ?>,

        },

        mounted : function(){

        },

        computed : {},
        methods : {

            /* Récupération du Nom du Produit */
            getProductName : function(id){
                for(var i=0;i<this.productTypes.length;i++){
                    if(this.productTypes[i].value ==id)
                        return this.productTypes[i].label;
                }
            }

        }

    });



</script>




<?php

    use Cake\Datasource\ConnectionManager;

    /* TOTAL CLIENTS */
    $clientsCount = 155;

    /* TRANSACTIONS EN PENDING */
    $transactionsCount = 200;

    /* COMMANDES EN ATTENTE DE VALIDATION */
    $ordersCount = 133;


    /* Récupérer le nombre de jours dans un mois */
    $daysCount = cal_days_in_month(CAL_GREGORIAN,Date('m'),Date('Y'));


    $dayslist     = [];
    $dayslistData = [];

    /* Création des journée d'un calendrier */

    for($i=1; $i<=$daysCount; $i++)
    {
        $dayslist[] = $i;
        $dayslistData[(int)$i] = rand(0,15000);
    }


?>

<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Tableau de bord</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"></div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row row-in">
                            <div class="col-lg-4 col-sm-6 row-in-br">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="counter text-right m-t-15"><?= ((int)$clientsCount) ?></h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>DETENTEURS <br /><b>DE CARTES</b></h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-4 col-sm-6 row-in-br  b-r-none">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="counter text-right m-t-15"><?= $transactionsCount ?></h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>TRANSACTIONS <br /><b>EFFECTUEES</b></h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-4 col-sm-6 b-0">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="counter text-right m-t-15"><?= $ordersCount ?></h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>COMMANDES <br /><b>EN ATTENTE</b> </h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STATS WAPS -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row row-in">


                            <div class="col-lg-4 col-sm-4 row-in-br">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="text-right m-t-15">
                                            <?= number_format(h((int)2300), 0, ',', ' ') ?>
                                        </h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>CREDITS <br /><b>COMMANDES</b></h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-4 col-sm-4 row-in-br  b-r-none">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="text-right m-t-15">
                                            <?= number_format(h((int)1300), 0, ',', ' ') ?>
                                        </h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>CREDITS <br /><b>CONSOMMES</b></h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-lg-4 col-sm-4 b-0">
                                <ul class="col-in">
                                    <li class="col-last">
                                        <h3 class="text-right m-t-15">
                                            <?= number_format(h((int)500), 0, ',', ' ') ?>
                                        </h3>
                                    </li>
                                    <li class="col-middle">
                                        <h4>CREDITS <br /><b>COMMISSIONS</b></h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


            <!--row -->
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="white-box">
                        <h3 class="text-center box-title">CONSOMATION CREDITS DU MOIS ENCOURS</h3>
                        <div id="transactions" style="height: 315px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?= $this->Html->script('/ample/plugins/bower_components/jquery/dist/jquery.min.js') ?>

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>


<!--Morris JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/raphael/raphael-min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/morrisjs/morris.js') ?>

<!-- chartist chart -->
<?= $this->Html->script('/ample/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>


<!-- Calendar JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/moment/moment.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/calendar/dist/cal-init.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom') ?>
<?= $this->Html->script('/ample/js/dashboard1.js') ?>

<script type="text/javascript">

    $(document).ready(function(){

        /* TRANSACTIONS DU MOIS */
        new Chartist.Line('#transactions', {
            labels: <?= json_encode($dayslist) ?>,
            series: [
                <?= json_encode(array_values($dayslistData)) ?>,
            ]
        }, {
            top: 0,
            low: 10,
            showPoint: true,
            fullWidth: true,
            plugins: [ Chartist.plugins.tooltip()],
            axisY: {
                labelInterpolationFnc: function (value) {

                    console.log(value);
                    return (value);
                }
            },
            showArea: true,
            axisY: {
                onlyInteger: true,
            },
        });

    })


</script>

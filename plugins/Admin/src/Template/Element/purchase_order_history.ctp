<div class="table-responsive">
    <table id="companies" class="table">
        <thead>
        <tr>
            <th>DATE</th>
            <th>Quantité</th>
            <th>Etat</th>
            <th>Remarques</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data as $d): ?>
            <tr>
                <td><?= h($d->created) ?></td>
                <td><?= h($d->qty) ?></td>
                <td>
                    <?php if($d->status == 'pending'): ?>
                      <span class="label label-warning label-rouded">En attente</span>
                    <?php elseif($d->status == 'valid'): ?>
                      <span class="label label-warning label-rouded">Validé</span>
                    <?php elseif($d->status == 'refused'): ?>
                      <span class="label label-warning label-rouded">Refusé</span>
                    <?php elseif($d->status == 'canceled'): ?>
                      <span class="label label-warning label-rouded">Annulé</span>
                    <?php endif; ?>
                </td>
                <td><?= h($d->remarks) ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

    <!-- Nouvelle Table -->
    <div class="row">
        <div class="col-sm-7">
            <h5>FACTURES <span class="label label-table label-info"><?= $data->count() ?></span></h5>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-3 text-right">
            <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn waves-effect btn-wj waves-light btn-info"><i class="fa fa-search"></i></button>
            </span>
                <input type="text" id="orders-search"  class="form-control" placeholder="Chercher ...">
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="orders" class="table table-striped">
                    <thead>
                    <tr>
                        <th class="text-left">ID</th>
                        <th class="text-left">REFERENCE</th>
                        <th class="text-left">ENTREPRISE</th>
                        <th class="text-center">STATUT</th>

                        <th class="text-left">DATE</th>

                        <th class="text-right">ACTIONS</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($data as $d): ?>
                        <tr>
                            <td><?= h($d->id) ?></td>
                            <td>
                                <a href="<?= $this->Url->build(['action'=>'view',$d->id]) ?>" ><?= h(strtoupper($d->ref)) ?></a>
                            </td>
                            <td class="text-left">
                                <a href="<?= $this->Url->build(['action'=>'view',$d->id]) ?>" ><?= h(strtoupper($d->company->denomination)) ?></a>
                            </td>
                            <td class="text-center">
                                <?php if($d->status == 'draft'): ?>
                                    <span class="label label-warning label-rouded">BROUILLON</span>
                                <?php elseif($d->status == 'confirmed'): ?>
                                    <span class="label label-success label-rouded">CONFIRME</span>
                                <?php endif; ?>
                            </td>
                            <td><?= h($d->created->i18nFormat('dd/M/Y HH:mm')) ?></td>

                            <!-- Actions -->
                            <td class="text-right">
                                <?php if($d->status == 'pending') : ?>

                                    <a href="<?= $this->Url->build(['action'=>'process',$d->id,'valid']) ?>"  class="btn btn-success btn-sm">Valider</a>
                                    <a href="<?= $this->Url->build(['action'=>'process',$d->id,'canceled']) ?>"  class="btn btn-warning btn-sm">Annuler</a>
                                    <a href="<?= $this->Url->build(['action'=>'process',$d->id,'refused']) ?>"  class="btn btn-danger btn-sm">Refuser</a>

                                <?php else: ?>

                                    <a href="javascript:void(0)"  class="btn btn-default btn-sm">Valider</a>
                                    <a href="javascript:void(0)"  class="btn btn-default btn-sm">Annuler</a>
                                    <a href="javascript:void(0)"  class="btn btn-default btn-sm">Refuser</a>

                                <?php endif; ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <style type="text/css">

        input[type="search"] {

            padding: 8px;

        }

        table.dataTable thead th, table.dataTable thead td {
            padding: 10px 18px;
            border-bottom:none;
        }

    </style>


    <script>
        $(document).ready(function() {

            var companiesTable = $('#orders').DataTable({
                dom: 'Bfrtip',
                searching : true,

                /* Cacher la colonne de l'identifiant */
                columnDefs: [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ],
                order : [[ 0, "desc" ]],
                language: {
                    search: "",
                    searchPlaceholder: "Recherche ...",
                    processing:     "Traitement en cours...",
                    lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate:
                        {
                            next : "Suivant",
                            previous:"Précédent",
                            entries : "entrées",
                            showing : "affichage",
                        }

                }
            });

            $('#orders-search').keyup(function(){
                companiesTable.search($(this).val());
                companiesTable.draw();
            });

            $('#orders_filter').hide();


        });


    </script>








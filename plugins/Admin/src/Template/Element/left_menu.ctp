<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu"></span></h3> </div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?= $this->Url->build("/admin/dashboard") ?>">
                    <i class="mdi mdi-chart-bar fa-fw"></i><span class="hide-menu">TABLEAU DE BORD</span>
                </a>
            </li>
            <li class="devider"></li>

            <li>
                <a href="<?= $this->Url->build(['controller'=>'companies','action'=>'index','company']) ?>">
                    <i class="mdi mdi-domain"></i><span class="hide-menu">  ENTREPRISES</span>
                </a>
            </li>

            <li>
                <a href="<?= $this->Url->build(['controller'=>'companies','action'=>'index','rbac']) ?>">
                    <i class="mdi mdi-domain"></i><span class="hide-menu">  AUTORISATIONS</span>
                </a>
            </li>


            <li class="devider"></li>
            <li><a href="<?= $this->Url->build(['plugin'=>'authentication','controller'=>'users','action'=>'logout']) ?>"><i class="mdi mdi-logout fa-fw"></i><span class="hide-menu">DECONNEXION</span></a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <h5>ENTREPRISES <span class="label label-table label-info"><?= $data->count() ?></span></h5>
    </div>
    <div class="col-sm-2"></div>
    <div class="col-sm-3 text-right">
        <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn waves-effect btn-wj waves-light btn-info"><i class="fa fa-search"></i></button>
            </span>
            <input type="text" id="companies-search"  class="form-control" placeholder="Chercher ...">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table id="companies" class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>DENOMINATION</th>
                <th class="text-center">COMPTE</th>
                <th class="text-center">CARTE</th>
                <th class="text-center">CREDIT</th>
                <th class="text-center">RESERVE</th>
                <th class="text-center">RESERVE CN</th>
                <th class="text-right">ACTIONS</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data as $d): ?>
                <tr>


                    <td>
                        <?= $d->id ?>
                    </td>


                    <td>
                        <a href="<?= $this->Url->build(['action'=>'view',$d->id]) ?>" ><?= h(strtoupper($d->denomination)) ?></a>
                    </td>

                    <!-- Statut Compte ( Connexion ) -->
                    <td class="text-center">
                        <?php if($d->user->active): ?>
                            <i class="fa fa-check-square" aria-hidden="true" style="color: #4caf50"></i>
                        <?php else: ?>
                            <i class="mdi mdi-close-box" style="color: red"></i>
                        <?php endif; ?>
                    </td>

                    <!-- Statut Compte ( Paiement ) -->
                    <td class="text-center">
                        <?php if($d->account->allow_card_use): ?>
                            <i class="fa fa-check-square" aria-hidden="true" style="color: #4caf50"></i>
                        <?php else: ?>
                            <i class="mdi mdi-close-box" style="color: red"></i>
                        <?php endif; ?>
                    </td>

                    <!-- CREDIT -->
                    <td class="text-right">
                        <?= number_format(h($d->account->credit), 0, ',', ' ') ?>
                    </td>

                    <!-- RESERVE -->
                    <td class="text-right">
                        <?= number_format(h($d->account->exceeding_allowed_credit), 0, ',', ' ') ?>
                    </td>

                    <td class="text-right">
                        <?= number_format(h($d->account->exceeding_consumption_credit), 0, ',', ' ') ?>
                    </td>


                    <!-- Actions -->
                    <td class="text-right">
                        <a href="<?= $this->Url->build(['action'=>'login',$d->id]) ?>" target="_self" class="btn btn-wj btn-sm">CONNECTER</a>
                        <a href="<?= $this->Url->build(['action'=>'edit',$d->id]) ?>"  class="btn btn-default btn-sm">EDITER</a>
                        <?php if( ($d->company_type->name == 'company') && ($d->user->active) && ($d->account->allow_card_use) ): ?>
                            <a href="<?= $this->Url->build(['action'=>'accrediter',$d->id]) ?>"  class="btn btn-wj btn-sm">ACCREDITER</a>
                        <?php else: ?>
                            <a href="javascript:void(0)"  class="btn btn-default btn-sm">ACCREDITER</a>
                        <?php endif; ?>
                    </td>



                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
        </div>
    </div>
</div>




<style type="text/css">

    input[type="search"] {

        padding: 8px;

    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom:none;
    }

</style>


<script>
    $(document).ready(function() {

        var companiesTable = $('#companies').DataTable({
            dom: 'Bfrtip',
            searching : true,

            /* Cacher la colonne de l'identifiant */
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],

            order : [[ 0, "desc" ]],

            language: {
                search: "",
                searchPlaceholder: "Recherche ...",
                processing:     "Traitement en cours...",
                lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable:     "Aucune donnée disponible dans le tableau",
                paginate:
                    {
                        next : "Suivant",
                        previous:"Précédent",
                        entries : "entrées",
                        showing : "affichage",
                    }

            }
        });

        $('#companies-search').keyup(function(){
            companiesTable.search($(this).val());
            companiesTable.draw();
        });

        $('#companies_filter').hide();


    });


</script>


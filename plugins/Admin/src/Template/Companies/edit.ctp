<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">DETAILS DU CLIENT</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Form->create($company) ?>


            <div class="row">

                <div class="col-sm-8">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">ENTREPRISES</a></li>
                        <li class="active">EDITER</li>
                    </ol>
                </div>

                <div class="col-lg-2">
                    <a href="<?= $this->Url->build(['action'=> 'index',$company->company_type->name])?>" type="submit" class="btn btn-block btn-default pull-right">ANNULER</a>
                </div>

                <div class="col-lg-2">
                    <button type="submit" class="btn btn-block btn-wj pull-right">ENREGISTRER</button>
                </div>
            </div>


            <?= $this->Flash->render() ?>


            <div class="row">

                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Informations sur l'entreprise</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">


                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('denomination',['class'=>'form-control','id'=>'denomination','placeholder'=>'DENOMINATION','label'=>'* DENOMINATION']) ?>
                                        </div>
                                    </div>

                                    <!-- TYPE -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>TYPE</label>
                                            <p><?=($company->company_type->name) ?></p>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= $this->Form->input('address',['type'=>'text', 'class'=>'form-control','id'=>'address','placeholder'=>'ADRESSE','label'=>'* ADRESSE']) ?>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('email',['class'=>'form-control','id'=>'Email','placeholder'=>'EMAIL','label'=>'EMAIL']) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('mobile',['class'=>'form-control','id'=>'mobile','placeholder'=>'MOBILE','label'=>'MOBILE']) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('rc_n',['class'=>'form-control','id'=>'rc_n','placeholder'=>'REGISTRE DE COMMERCE','label'=>'REGISTRE DE COMMERCE']) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('iden_fiscal',['class'=>'form-control','id'=>'iden_fiscal','placeholder'=>'IDENTIFIANT FISCAL','label'=>'IDENTIFIANT FISCAL']) ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <hr />
                        <h3 class="box-title m-b-0">Informations sur la Géolocalisation</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('latitude',['class'=>'form-control','id'=>'latitude','placeholder'=>'Latitude','label'=>'Latitude','type'=>'number','step'=>'any']) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('longitude',['class'=>'form-control','id'=>'longitude','placeholder'=>'Longitude','label'=>'Longitude','type'=>'number','step'=>'any']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Informations sur le compte</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">


                                <div class="row">


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CONNEXION</label>
                                            <?= $this->Form->select('user.active'
                                                ,[0=>'NON AUTORISE',1=>'AUTORISE'],
                                                [
                                                    'multiple' => false,
                                                    'class'    => 'form-control',
                                                    'id'       => 'user_active',
                                                    'value'    => $company->user->active
                                                ]
                                            ) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>PAIEMENT PAR CARTES</label>
                                            <?= $this->Form->select('account.allow_card_use'
                                                ,[0=>'NON AUTORISE',1=>'AUTORISE'],
                                                [
                                                    'multiple' => false,
                                                    'class'    => 'form-control',
                                                    'id'       => 'user_active',
                                                    'value'    => $company->account->allow_card_use
                                                ]
                                            ) ?>
                                        </div>
                                    </div>


                                </div>


                                <div class="row">


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>COMMANDE</label>
                                            <?= $this->Form->select('account.allow_purchase'
                                                ,[0=>'NON AUTORISE',1=>'AUTORISE'],
                                                [
                                                    'multiple' => false,
                                                    'value'    => $company->account->allow_purchase,
                                                    'class'    => 'form-control',
                                                    'id'       => 'user_active'
                                                ]
                                            ) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>MONTANT LIVRAISON</label>
                                            <?= $this->Form->select('account.allow_delivery_price'
                                                ,[0=>'NON AUTORISE',1=>'AUTORISE'],
                                                [
                                                    'multiple' => false,
                                                    'class'    => 'form-control',
                                                    'id'       => 'user_active',
                                                    'value'    => $company->account->allow_delivery_price
                                                ]
                                            ) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>DETAILS LIVRAISON</label>
                                            <?= $this->Form->select('account.allow_delivery_details'
                                                ,[0=>'NON AUTORISE',1=>'AUTORISE'],
                                                [
                                                    'multiple' => false,
                                                    'class'    => 'form-control',
                                                    'id'       => 'user_active',
                                                    'value'    => $company->account->allow_delivery_details
                                                ]
                                            ) ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('user.identity',['class'=>'form-control','id'=>'user_identity','placeholder'=>'ID CLIENT','label'=>'ID CLIENT']) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('user.pin_code',['class'=>'form-control','id'=>'user_pin','placeholder'=>'CODE PIN','label'=>'CODE PIN']) ?>
                                        </div>
                                    </div>
                                </div>
                                <p class="text-muted m-b-30 font-13" style="color: red"><b>ID Client</b> &amp; <b>Code PIN</b> Servent comme username et password pour la connexion</p>


                                <?php if($company->company_type->name == 'company'): ?>
                                <div>

                                    <hr />

                                    <h3 class="box-title m-b-0">Informations sur l'accréditation</h3>
                                    <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <?= $this->Form->input('account.credit',
                                                    [
                                                        'class' => 'form-control',
                                                        'id'    => 'credit',
                                                        'placeholder' => 'CREDIT',
                                                        'label' => 'CREDIT' ,
                                                        'type' => 'text'
                                                    ]) ?>
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <?= $this->Form->input('account.notifications_threshold',
                                                    [
                                                        'class' => 'form-control',
                                                        'id'    => 'notifications_threshold',
                                                        'placeholder' => 'SEUIL DE NOTIFICATION',
                                                        'label' => 'SEUIL DE NOTIFICATION' ,
                                                        'type' => 'text',
                                                    ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted m-b-30 font-13"><b>Le Seuil de notification</b> permet au client d'etre notifié si jamais son crédit passe <br />sous la barre du seuil</p>


                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <?= $this->Form->input('account.exceeding_allowed_credit',
                                                    [
                                                        'class' => 'form-control',
                                                        'id'    => 'exceeding_allowed_credit',
                                                        'placeholder' => 'CREDIT DEPASSEMENT',
                                                        'label' => 'CREDIT DEPASSEMENT' ,
                                                        'type' => 'text',
                                                    ]) ?>
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <?= $this->Form->input('account.exceeding_consumption_credit',
                                                    [
                                                        'class' => 'form-control',
                                                        'id'    => 'exceeding_consumption_credit',
                                                        'placeholder' => 'TOTAL DEPASSEMENT',
                                                        'label' => 'TOTAL DEPASSEMENT' ,
                                                        'type' => 'text',
                                                    ]) ?>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                <?php endif; ?>


                                <!-- Afficher uniquement si Réstaurant -->
                                <?php if( ( $company->company_type->name == 'restaurant' || $company->company_type->name == 'hotel' ) && $company->account->total_to_pay == 0 ): ?>
                                    <div>
                                        <hr />
                                        <h3 class="box-title m-b-0">Informations sur les transactions</h3>
                                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                                        <div class="row">

                                            <!-- COMMISSION -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>COMMISSION (%)</label>
                                                    <?= $this->Form->control('commission',
                                                        [
                                                            'class'    => 'form-control',
                                                            'id'       => 'restaurant_commission',
                                                            'label'    => false
                                                        ]
                                                    ) ?>
                                                </div>
                                            </div>

                                            <!-- DISCOUNT -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>REMISE (%)</label>
                                                    <?= $this->Form->control('discount',
                                                        [
                                                            'class'    => 'form-control',
                                                            'id'       => 'restaurant_discount',
                                                            'label'    => false
                                                        ]
                                                    ) ?>
                                                </div>
                                            </div>

                                        </div>
                                        <p class="text-muted m-b-30 font-13" style="color: red">Note : Vous devez solder le compte avant de changer la commission et la remise</p>
                                    </div>
                                <?php elseif(  ( $company->company_type->name == 'restaurant' || $company->company_type->name == 'hotel' ) && $company->account->total_to_pay > 0 ): ?>
                                    <div>
                                        <hr />
                                        <h3 class="box-title m-b-0">Informations sur les transactions</h3>
                                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                                        <div class="row">

                                            <!-- COMMISSION -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>COMMISSION (%)</label>
                                                    <p><?= $company->commission ?> %</p>
                                                </div>
                                            </div>

                                            <!-- DISCOUNT -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>REMISE (%)</label>
                                                    <p><?= $company->discount ?> %</p>
                                                </div>
                                            </div>

                                        </div>
                                        <p class="text-muted m-b-30 font-13" style="color: red">Note : Vous devez solder le compte avant de changer la commission et la remise</p>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/vue.min') ?>
<?= $this->Html->script('/js/jquery.maskMoney.min') ?>
<?= $this->Html->script('/js/jquery.inputmask.bundle.min') ?>

<style type="text/css">

    .error-message {
        color:red;
        padding: 1em;
        padding-left: 0;
    }

    input.form-error {
        border :1px solid red;

    }

</style>


<script type="text/javascript">


    /* AutoComplete de l'adresse et conversion en Longitude Lattitude */
    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('address')),
            {
                types: ['geocode'],
                componentRestrictions: {country: "dz"}
            },

        );

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace().geometry.location;

        $('#latitude').val(place.lat());
        $('#longitude').val(place.lng());

    }

    /* Masque de saisie Money */
    $('#credit').maskMoney({
        thousands:' ',
        allowZero:true,
        allowNegative : false,
        precision :0
    });

    $('#notifications_threshold').maskMoney({
        thousands:' ',
        allowZero:true,
        allowNegative : false,
        precision :0
    });

    $('#exceeding_allowed_credit').maskMoney({
        thousands:' ',
        allowZero:true,
        allowNegative : false,
        precision :0
    });

    $('#exceeding_consumption_credit').maskMoney({
        thousands:' ',
        allowZero:true,
        allowNegative : false,
        precision :0
    });




    $('#rc_n').inputmask("99-a-9999999");
    $('#iden_fiscal').inputmask("999999999999999");


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGt1Xc6EIEyiddx_-fQp6grRqBQIbpa7w&libraries=places&callback=initAutocomplete"></script>

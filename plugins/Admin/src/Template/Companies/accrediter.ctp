<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <?= $this->Form->create($accForm) ?>

            <?= $this->Flash->render() ?>


            <div class="vertical-center">


                <div class="row">

               <div class="col-sm-3"></div>
                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Montant à transférer</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>

                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= $this->Form->control('amount',['class'=>'form-control','id'=>'amount','placeholder'=>'* MONTANT EN CRÉDITS','label'=>'* MONTANT EN CRÉDITS']) ?>
                                </div>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="row text-center">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-block btn-wj pull-right">VALIDER</button>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                    </div>

                </div>
                <div class="col-sm-3"></div>


            </div>

            </div>

            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/jquery.maskMoney.min') ?>

<style type="text/css">

    .error-message {
        color:red;
        padding: 1em;
        padding-left: 0;
    }

    input.form-error {
        border :1px solid red;

    }

    .vertical-center {

        position: absolute;
        left: 50%;
        top: 50%;
        width: 100%;
        transform: translate(-50%,-50%);
        padding-top: 30px;

    }


</style>

<script type="text/javascript">

    $('#amount').maskMoney({
        thousands:' ',
        allowZero:false,
        allowNegative : false,
        precision :0
    });

</script>


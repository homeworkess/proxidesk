<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Liste des hotels</h4> </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Flash->render() ?>

            <div class="row">

                <div class="col-sm-10">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">HOTELS</a></li>
                        <li class="active">LISTE</li>
                    </ol>
                </div>

                <div class="col-lg-2">
                    <a href="<?= $this->Url->build(['action'=>'add','hotel']) ?>" class="btn btn-block btn-wj pull-right">NOUVEAU</a>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <?= $this->Element('Admin.hotels_table',['data'=>$companies]) ?>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/datatables/media/js/jquery.dataTables.min',['block'=>'scripts_header']) ?>
<?= $this->Html->script('/ample/plugins/bower_components/bootstrap-select-1.12.4/dist/js/bootstrap-select.min',['block'=>'scripts_header']) ?>


<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->css('/ample/plugins/bower_components/datatables/media/css/jquery.dataTables.min',['block'=>'css_header']) ?>

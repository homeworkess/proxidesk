<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">INFORMATIONS SUR L'ENTREPRISE</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Form->create($company) ?>

            <div class="row">
                <div class="col-sm-10">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">ENTREPRISES</a></li>
                        <li class="active">DETAILS</li>
                    </ol>
                </div>

                <div class="col-lg-2">
                    <a href="<?= $this->Url->build(['action'=>'index',$company->company_type->name])?>" type="submit" class="btn btn-block btn-default pull-right">RETOUR</a>
                </div>

            </div>


            <?= $this->Flash->render() ?>


            <div class="row">

                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Informations sur l'entreprise</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">


                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Dénomination</label>
                                            <p><?= h($company->denomination) ?></p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CATEGORIE</label>
                                            <p>
                                                <?= h(strtoupper(

                                                        $company->company_type->name == 'company' ? 'Entreprise' : $company->company_type->name

                                                )) ?>
                                            </p>
                                        </div>
                                    </div>


                                </div>


                                <div class="form-group">
                                    <label>Address</label>
                                    <p><?= $company->address ?></p>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>EMAIL</label>
                                            <p><?= h($company->email ? $company->email : 'N.C')  ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>MOBILE</label>
                                            <p><?= h($company->mobile ? $company->mobile : 'N.C') ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Registre de commerce</label>
                                            <p><?= h($company->rc_n) ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Identifiant Fiscal</label>
                                            <p><?= h($company->iden_fiscal) ?></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>



                </div>
                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Informations sur le compte</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">


                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>IDENTIFIANT UNIQUE :</label>
                                            <p style="font-size: 16px">
                                                <span class="label label-success"><?= h($company->unique_id) ?></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CONNEXION</label>
                                            <p><?= h($company->user->active ? 'AUTORISE' : 'NON AUTORISE') ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>PAIEMENT PAR CARTES</label>
                                            <p><?= h($company->account->allow_card_use ? 'AUTORISE' : 'NON AUTORISE') ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>ID CLIENT</label>
                                            <p><?= h($company->user->identity) ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CODE PIN</label>
                                            <p>******</p>
                                        </div>
                                    </div>
                                </div>
                                <p class="text-muted m-b-30 font-13"><b>ID Client</b> &amp; <b>Code PIN</b> Servent comme username et password pour la connexion</p>

                                <?php if($company->company_type->name == 'company'): ?>

                                    <!-- informations sur l'entreprise -->
                                    <div>
                                    <hr />

                                    <h3 class="box-title m-b-0">Informations sur l'accréditation</h3>
                                    <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CREDIT DISPONIBLE</label>
                                                <p><?= h($company->account->credit) ?></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>SEUIL DE NOTIFICATION</label>
                                                <p><?= h($company->account->notifications_threshold) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <!-- SI C UN RESTAURANT OU HOTEL OU SPA -->
                                <?php if( ( $company->company_type->name == 'restaurant' || $company->company_type->name == 'hotel' )): ?>
                                <div>
                                    <hr />
                                    <h3 class="box-title m-b-0">Informations sur les transactions</h3>
                                    <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                                    <div class="row">

                                        <!-- COMMISSION -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>COMMISSION (%)</label>
                                                <p><?= h((float)$company->commission) ?></p>
                                            </div>
                                        </div>

                                        <!-- DISCOUNT -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>REMISE (%)</label>
                                                <p><?= h((float)$company->discount) ?></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php endif; ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>

<style type="text/css">

    .error-message {
        color:red;
        padding: 1em;
        padding-left: 0;
    }

    input.form-error {
        border :1px solid red;

    }

</style>

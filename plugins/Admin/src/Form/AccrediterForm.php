<?php
namespace Admin\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * Accrediter Form.
 */
class AccrediterForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->notEmpty('amount', 'Le montant ne peut etre vide', 'create')
            ->add('amount', 'custom', [
            'rule' => function ($value, $context) {
                return (is_integer((int)$value) && $value>0 && $value< 1000000);
                },
            'message' => 'Entrez un montant compris entre 100 & 1M'
        ]);
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}

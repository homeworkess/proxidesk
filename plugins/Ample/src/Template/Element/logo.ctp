<a class="logo" href="index.html">
    <!-- Logo icon image, you can use font-icon also -->
    <b>
        <!--This is dark logo icon-->
        <img src="<?= $this->Url->build('/ample/plugins/images/admin-logo.png') ?>" alt="home" class="dark-logo" />
        <!--This is light logo icon-->
        <img src="<?= $this->url->build('/ample/plugins/images/admin-logo-dark.png') ?>" alt="home" class="light-logo" />
    </b>
    <!-- Logo text image you can use text also -->
    <span class="hidden-xs">
        <!--This is dark logo text-->
        <img src="<?= $this->url->build('/ample/plugins/images/admin-text.png') ?>" alt="home" class="dark-logo" />
        <!--This is light logo text-->
        <img src="<?= $this->url->build('/ample/plugins/images/admin-text-dark.png') ?>" alt="home" class="light-logo" />
    </span>
</a>

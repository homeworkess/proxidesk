<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="ample/plugins/images/favicon.png">
    <title>IMMO MVP 0.1</title>

    <!-- Bootstrap Core CSS -->
    <?= $this->Html->css('/ample/bootstrap/dist/css/bootstrap.min.css') ?>

    <?= $this->Html->css('/ample/plugins/bower_components/datatables/media/css/jquery.dataTables.min') ?>
    <?= $this->Html->css('https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css') ?>

    <!-- This is Sidebar menu CSS -->
    <?= $this->Html->css('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>

    <!-- This is a Animation CSS -->
    <?= $this->Html->css('/ample/css/animate.css') ?>

    <!-- This is a Custom CSS -->
    <?= $this->Html->css('/ample/css/style.css') ?>
    <?= $this->Html->css('/ample/css/colors/default.css',['id'=>'theme']) ?>


    <!-- JS FOOTER -->
    <?= $this->fetch('css_header') ?>
    <?= $this->fetch('scripts_header') ?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body  class="fix-header">

    <!-- Contenu principal -->
    <?= $this->fetch('content') ?>

    <!-- JS FOOTER -->
    <?= $this->fetch('footer-js-assets') ?>


    <script type="text/javascript">


        // Theme settings
        $(".open-close").click(function () {
            $("body").toggleClass("show-sidebar");
        });
        //Open-Close-right sidebar
        $(".right-side-toggle").click(function () {
            $(".right-sidebar").slideDown(50);
            $(".right-sidebar").toggleClass("shw-rside");
            // Fix header
            $(".fxhdr").click(function () {
                $("body").toggleClass("fix-header");
            });
            // Fix sidebar
            $(".fxsdr").click(function () {
                $("body").toggleClass("fix-sidebar");
            });
            // Service panel js
            if ($("body").hasClass("fix-header")) {
                $('.fxhdr').attr('checked', true);
            }
            else {
                $('.fxhdr').attr('checked', false);
            }
        });


        //Loads the correct sidebar on window load,
        //collapses the sidebar on window resize.
        // Sets the min-height of #page-wrapper to window size
        $(function () {
            $(window).bind("load resize", function () {
                topOffset = 60;
                width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
                if (width < 768) {
                    $('div.navbar-collapse').addClass('collapse');
                    topOffset = 100; // 2-row-menu
                }
                else {
                    $('div.navbar-collapse').removeClass('collapse');
                }
                height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
                height = height - topOffset;
                if (height < 1) height = 1;
                if (height > topOffset) {
                    $("#page-wrapper").css("min-height", (height) + "px");
                }
            });
            var url = window.location;
            var element = $('ul.nav a').filter(function () {
                return this.href == url || url.href.indexOf(this.href) == 0;
            }).addClass('active').parent().parent().addClass('in').parent();
            if (element.is('li')) {
                element.addClass('active');
            }
        });




    </script>


    <style type="text/css">


        .message .success {

            background: #a4cd00;

        }

        .navbar-header {
            background:#109bf8 ;
        }


        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {

            background-color:#109bf8 ;
        }



    </style>


</body>

</html>
